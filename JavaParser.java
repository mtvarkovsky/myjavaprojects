package com.company;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Paths;
import java.nio.file.Path;
import java.util.stream.Collectors;

public class JavaParser {
    private Path path;
    public StringBuffer code;

    JavaParser(String code) {
        this.code = new StringBuffer(code);
        this.inputType = "string";
    }

    JavaParser(String fileLocation, Charset encoding) {
        Path path = Paths.get(fileLocation);

        String file = null;
        try {
            file = java.nio.file.Files.lines(path, encoding).collect(Collectors.joining());
        } catch (IOException e) {
            e.printStackTrace();
        }

        this.code = new StringBuffer(file);
        this.inputType = "file";
        this.path = path;
    }

    private void parse() {
        for (int i = 0; i < code.length() - 1; i++) {
            if ((code.charAt(i) == '{' || code.charAt(i) == '}' || code.charAt(i) == ';') && code.charAt(i+1) != '\n') {
                this.code.insert(i+1, '\n');
            }

            if ( (code.charAt(i+1) == '(' && code.charAt(i) != ' ') || (code.charAt(i+1) == '{' && code.charAt(i) != ' ') ) {
                this.code.insert(i+1, ' ');
            }

            if (code.charAt(i) == '/' && code.charAt(i+1) == '/') {
                do {
                    i++;
                } while(code.charAt(i) != '\n');
            }

            if (code.charAt(i) == '/' && code.charAt(i+1) == '*') {
                int j = i;

                do {
                    j++;
                } while(code.charAt(j) != '*' && code.charAt(j+1) != '/');
            }

            if (code.charAt(i) == '"') {
                do {
                    i++;
                } while(code.charAt(i) != '"');
            }

            if (code.charAt(i) == '\'') {
                do {
                    i++;
                } while(code.charAt(i) != '\'');
            }
        }
    }

    public String parseString() {
        parse();
        return code.toString();
    }

    public void parseFile() {
        parse();
        try {
            java.nio.file.Files.write(path, code.toString().getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
